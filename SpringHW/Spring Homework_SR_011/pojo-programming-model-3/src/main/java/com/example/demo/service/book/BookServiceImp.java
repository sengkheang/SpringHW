package com.example.demo.service.book;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.BookModel;
import com.example.demo.repository.article.BookRepository;

@Service
public class BookServiceImp implements BookService{

	private BookRepository bookRespositories;
	@Autowired
	public BookServiceImp(BookRepository bookRespositories) {
		this.bookRespositories = bookRespositories;
	}

	@Override
	public void add(BookModel bookModel) {
		bookRespositories.add(bookModel);
	}

	@Override
	public BookModel findOne(int id) {
		
		return bookRespositories.findOne(id);
	}

	@Override
	public List<BookModel> findAll() {
		return bookRespositories.findAll();
	}

	@Override
	public void delete(int id) {
		bookRespositories.delete(id);
		
	}
	
	@Override
	public void update(BookModel bookUS) {
		bookRespositories.update(bookUS);
		
	}

}
