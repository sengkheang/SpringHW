package com.example.demo.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class BookModel {
	
	//validate id using annotation
	@NotNull 
	private int id;
	
	@NotBlank
	private String title;
	
	@NotBlank
	private String bdescription;
	
	@NotBlank
	@Size(min=7, max=30)
	private String authorName;
	
	private String date;
	
	
	private String thumbnail;

	private Category category;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBdescription() {
		return bdescription;
	}

	public void setBdescription(String bdescription) {
		this.bdescription = bdescription;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "BookModel [id=" + id + ", title=" + title + ", bdescription=" + bdescription + ", authorName="
				+ authorName + ", date=" + date + ", thumbnail=" + thumbnail + ", category=" + category + "]";
	}

	public BookModel(int id, String title, String bdescription, String authorName, String date, String thumbnail,
			Category category) {
		super();
		this.id = id;
		this.title = title;
		this.bdescription = bdescription;
		this.authorName = authorName;
		this.date = date;
		this.thumbnail = thumbnail;
		this.category = category;
	}

	public BookModel() {
		super();
	}
	
	
}
