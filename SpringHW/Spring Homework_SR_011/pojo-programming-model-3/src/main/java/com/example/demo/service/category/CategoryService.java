package com.example.demo.service.category;

import java.util.List;

import com.example.demo.model.BookModel;
import com.example.demo.model.Category;

public interface CategoryService {
	List<Category> findAll();
	Category findOne(int id);
	
}
