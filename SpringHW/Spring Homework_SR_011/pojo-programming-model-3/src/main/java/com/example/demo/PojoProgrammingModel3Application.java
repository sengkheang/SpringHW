package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PojoProgrammingModel3Application {

	public static void main(String[] args) {
		SpringApplication.run(PojoProgrammingModel3Application.class, args);
	}
}
