create table tbl_categories
(
  id   serial not null
    constraint tbl_categories_pkey
    primary key,
  name varchar(50)
);

-- auto-generated definition
create table tbl_articles
(
  id           serial not null
    constraint tbl_articles_pkey
    primary key,
  title        varchar(50),
  bdescription varchar(100),
  thumbnail    varchar(100),
  authorname   varchar(50),
  date         varchar(50),
  category     integer
    constraint tbl_articles_category_fkey
    references tbl_categories
);

INSERT INTO tbl_categories (name) VALUES ('Data-Science');
INSERT INTO tbl_categories (name) VALUES ('Business');
INSERT INTO tbl_categories (name) VALUES ('Self-Development');
INSERT INTO tbl_categories (name) VALUES ('Languages');
INSERT INTO tbl_categories (name) VALUES ('Computer-Science');
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);
INSERT into tbl_articles (title, bdescription, authorname, thumbnail, date, category) values ('Data_Science', 'Home', 'MEssi', 'image/profile.jpg', '06/26/2018', 1);