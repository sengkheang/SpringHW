package com.example.demo.repository.category;

import java.util.List;



import com.example.demo.model.Category;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository {
	@Select("SELECT * FROM tbl_categories")
	List<Category> findAll();
	@Select("SELECT * FROM tbl_categories where id=#{id}")
	Category findOne(int id);
}
