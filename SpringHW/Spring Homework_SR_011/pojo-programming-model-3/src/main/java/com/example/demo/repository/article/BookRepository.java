package com.example.demo.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import com.example.demo.model.BookModel;
@Repository
public interface BookRepository {
    @Insert("INSERT into tbl_articles (title, bdescription, thumbnail, authorname, date, category) Values (#{title}, #{bdescription}, #{thumbnail}, #{authorName}, #{date}, #{category.id});")
    @Select("Select * from tbl_articles")
    @Results({
            @Result(property ="category.name", column="name"),
            @Result(property ="category.id", column="id"),
            @Result(property ="id", column="id"),
    })
    void add(BookModel bookModel);

    @Select("SELECT a.id, a.title, a.bdescription, a.thumbnail, a.authorname, a.date, a.category,c.name FROM public.tbl_articles as a left join tbl_categories as c on c.id=a.category where a.id=#{id}")
    @Results({
            @Result(property ="category.name", column="name"),
            @Result(property ="category.id", column="id"),
            @Result(property ="id", column="id"),
    })
	BookModel findOne(int id);

	@Select("SELECT a.id, a.title, a.bdescription, a.thumbnail, a.authorname, a.date, a.category,c.name FROM public.tbl_articles as a inner join tbl_categories as c on c.id=a.category Order By a.id ASC")
	@Results({
            @Result(property ="category.name", column="name"),
            @Result(property ="category.id", column="id"),
            @Result(property ="id", column="id"),
    })
    List<BookModel> findAll();

	@Delete("Delete from tbl_articles  where id=#{id}")
	void delete(int id);
	//create new update method by using Article object

    @Update("update tbl_articles set title =#{title}, bdescription =#{bdescription},thumbnail=#{thumbnail}, authorname=#{authorName},category=#{category.id} where id=#{id}")
	void update(BookModel bookUR);
}	
