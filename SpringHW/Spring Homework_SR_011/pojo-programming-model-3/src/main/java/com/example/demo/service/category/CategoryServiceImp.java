package com.example.demo.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.model.BookModel;
import com.example.demo.model.Category;
import com.example.demo.repository.category.CategoryRepository;

@Repository
public class CategoryServiceImp implements CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepos;

	@Override
	public List<Category> findAll() {
		return categoryRepos.findAll();
	
	}

	@Override
	public Category findOne(int id) {
		return categoryRepos.findOne(id);
	}

	

}
