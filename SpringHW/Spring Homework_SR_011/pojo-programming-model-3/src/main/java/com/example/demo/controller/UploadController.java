package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;



@Controller
//@PropertySource("classpath:ams.properties")
public class UploadController {
//	
//	@Value("${file.upload.server.path}")
//	private String serverPath;
	
	@GetMapping("/upload")
	public String upload() {
		return "upload";
	}
	
	@PostMapping("/upload")
	public String postUpload(@RequestParam("files") List<MultipartFile> mfs) {
		String serverPath="C://Users/SR_020/Desktop/PhotoUploaded/";
		
		if(!mfs.isEmpty()) {
			mfs.forEach(file ->{
				try {
					//UUID to rename file
					//String filename=UUID.randomUUID().toString()+ mf.getOriginalFilename();
					String filename= UUID.randomUUID()+ "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
					Files.copy(file.getInputStream(), Paths.get(serverPath, filename));
					System.out.println(filename);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			
		}
		
		//print to get file by our filename
		System.out.println();
		return "redirect:/upload";
	}
}
