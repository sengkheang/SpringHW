package com.example.demo.controller;




import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import com.example.demo.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.BookModel;
import com.example.demo.service.book.BookService;
import com.example.demo.service.category.CategoryService;

@Controller
public class BookControllers {
//	@GetMapping
//	public String article() {
//		return "article";
//	}
	@Autowired
	private BookService bookServices; 
	@Autowired
	private CategoryService categoryServices;
	
	@Value("${file.upload.server.path}")
	private String serverPath;
	
	@GetMapping("/article")
	public String article(ModelMap map) {
		List<BookModel> bookModel=bookServices.findAll();
		System.out.println(bookModel);
		System.out.println(categoryServices.findAll());
		map.addAttribute("articles",bookModel);
		
		
		return "article";
	}
	
	@GetMapping("/add")
	public String add(ModelMap map,BookModel book) {
        List<Category> category=categoryServices.findAll();
		map.addAttribute("book", book);
		
		//let us know that we are in form add to reduce creating more form 
		map.addAttribute("FormAdd", true);
		map.addAttribute("categories",category);
		System.out.println(categoryServices.findAll());
		return "add";
	}
	
	@PostMapping("/add")
	/*public String saveBook(@Valid @ModelAttribute BookModel book, ModelMap map, BindingResult result) {*/
		public String saveBook(@RequestParam("image") MultipartFile thumbnail,@Valid @ModelAttribute BookModel book, BindingResult result,ModelMap map ) {

		String generatePath=null;
		System.out.println("Erro: "+result.hasErrors());
		if(result.hasErrors()) {
			//return value to object book bcus we test the value validation on form add first
			map.addAttribute("book", book);
			map.addAttribute("categories",categoryServices.findAll());
			map.addAttribute("FormAdd", true);
			return "add";
		}
		if(!thumbnail.isEmpty()){ 
			
			try {
					
				generatePath=UUID.randomUUID().toString() + "." +thumbnail.getOriginalFilename().substring(thumbnail.getOriginalFilename().lastIndexOf("."));
				Files.copy(thumbnail.getInputStream(), Paths.get(serverPath, generatePath));
			
			}catch(IOException e) {
				e.printStackTrace();
			}
			
			
		}

		System.out.println("Pring: "+book);
		System.out.println("imge: "+thumbnail.getOriginalFilename());
		
		book.setThumbnail("/image/"+ generatePath);
		book.setCategory(categoryServices.findOne(book.getCategory().getId()));
		book.setDate(new Date().toString());
		bookServices.add(book);
		return "redirect:/article";
	}
	
	//getmapping on webpage to /delete/{id} as dynamic
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id ) {
		bookServices.delete(id);
		return "redirect:/article";
	}
	
	@GetMapping("/update/{id}")
	public String update(ModelMap map, @PathVariable int id) {
		List<Category> category=categoryServices.findAll();
		map.addAttribute("book", bookServices.findOne(id));
		System.out.println("aaaaaaaaaaaaaaaaaaaa 1="+bookServices.findOne(id).getThumbnail());
		
		//let us know that we are  in form update not Add to reduce creating more form 
		map.addAttribute("FormAdd", false);

		map.addAttribute("categories",category);
		return "update";
	}
	
	@PostMapping("/update")
	//@modelattribute used for binding date into new object book
	public String updateP(@RequestParam("image") MultipartFile thumbnail, @ModelAttribute BookModel book ) {
		
		
		System.out.println("image: "+book);
		
		String generatePath=null;
		if(!thumbnail.isEmpty()){ 
			
			try {
					
				generatePath=UUID.randomUUID().toString() + "." +thumbnail.getOriginalFilename().substring(thumbnail.getOriginalFilename().lastIndexOf("."));
				Files.copy(thumbnail.getInputStream(), Paths.get(serverPath, generatePath));
				book.setThumbnail("/image/"+generatePath);
			
			}catch(IOException e) {
				e.printStackTrace();
			}				
		}
//		BookModel b=bookServices.findOne(book.getId());
		book.setDate(new Date().toString());
		
		System.out.println("aaaaaaaaaaaaaaaaaaaa 2="+book.getThumbnail());
//		System.out.println(book);
		book.setCategory(categoryServices.findOne(book.getCategory().getId()));
		bookServices.update(book);
		return "redirect:/article";
	}
	
	
	@GetMapping("/view/{id}")
	public String view(ModelMap map, @PathVariable int id) {
		List<Category> category=categoryServices.findAll();
		map.addAttribute("categories",category);
		map.addAttribute("book", bookServices.findOne(id));
		return "view";
	}

	@GetMapping("/addCategory")
	public String addCategory(ModelMap map,BookModel book){
		List<Category> category=categoryServices.findAll();
		map.addAttribute("categories",category);
		return "addCategory";
	}


}
