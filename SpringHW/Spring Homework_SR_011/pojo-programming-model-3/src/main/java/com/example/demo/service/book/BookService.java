package com.example.demo.service.book;

import java.util.List;

import com.example.demo.model.BookModel;

public interface BookService {
	 void add(BookModel bookModel);
	BookModel findOne(int id);
	List<BookModel> findAll();
	void delete(int id);
	void update(BookModel bookUS);
}
