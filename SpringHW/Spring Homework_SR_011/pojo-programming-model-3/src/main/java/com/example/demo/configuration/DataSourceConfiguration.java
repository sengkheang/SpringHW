package com.example.demo.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DataSourceConfiguration {
	@Bean
	@Profile("Database")
	@Qualifier("dataSource")
	public DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource=new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
		driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/ArticleKh");
		driverManagerDataSource.setUsername("postgres");
		driverManagerDataSource.setPassword("jldr2424");
		return driverManagerDataSource;
	}

	@Bean
	@Profile("Memory")
	@Qualifier("dataSource")
	public DataSource dataSourceMemory() {
		EmbeddedDatabaseBuilder db=new EmbeddedDatabaseBuilder();
		db.setType(EmbeddedDatabaseType.H2);
		db.addScript("sql/table.sql");
		System.out.println(db.build().toString());
		return db.build();
	}
	
}
